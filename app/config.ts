/**
 * Configuartion des données de base de la carte Mapbox
 */

const CONFIG =  {
    accessToken: "pk.eyJ1IjoiYWxwaW5vc2RyZWFtIiwiYSI6ImNqc3VjYWhzbzF4bHc0NG8wdWxpcmc5b2oifQ.SaRfjFqW7daFY_SrfKMrNQ",
    container: "map",
    style: "mapbox://styles/alpinosdream/cjteerzem46r51fquywyr87cd"
}

export default CONFIG;