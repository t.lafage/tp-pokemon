export class Data {

    static getData(): Array<any> {
        return [
            {
                index: 25,
                name: 'Pikachu',
                description: 'Chaque fois que Pikachu découvre quelque chose de nouveau, il envoie un arc électrique. Lorsqu\'on tombe sur une Baie carbonisée, ça signifie sans doute qu\'un de ces Pokémon a envoyé une charge trop forte.',
                types: [ 'Électrik' ],
                PC: 938,
                health: 98
            },
            {
                index: 26,
                name: 'Raichu',
                description: 'Si ses joues contiennent trop d\'électricité, Raichu plante sa queue dans le sol pour se décharger. On trouve des parcelles de terre brûlée à proximité du nid de ce Pokémon.',
                types: [ 'Électrik' ],
                PC: 2182,
                health: 132
            },
            {
                index: 1,
                name: 'Bulbizarre',
                description: 'Bulbizarre passe son temps à faire la sieste sous le soleil. Il y a une graine sur son dos. Il absorbe les rayons du soleil pour faire doucement pousser la graine.',
                types: [ 'Plante', 'Poison' ],
                PC: 1115,
                health: 111
            },
            {
                index: 2,
                name: 'Herbizarre',
                description: 'Un bourgeon a poussé sur le dos de ce Pokémon. Pour en supporter le poids, Herbizarre a dû se muscler les pattes. Lorsqu\'il commence à se prélasser au soleil, ça signifie que son bourgeon va éclore, donnant naissance à une fleur.',
                types: [ 'Plante', 'Poison' ],
                PC: 1699,
                health: 132
            },
            {
                index: 3,
                name: 'Florizarre',
                description: 'Une belle fleur se trouve sur le dos de Florizarre. Elle prend une couleur vive lorsqu\'elle est bien nourrie et bien ensoleillée. Le parfum de cette fleur peut apaiser les gens.',
                types: [ 'Plante', 'Poison' ],
                PC: 2720,
                health: 160
            },
            {
                index: 317,
                name: 'Avaltout',
                description: 'Lorsqu\'Avaltout repère une proie, il libère une substance terriblement toxique de ses pores et en enduit son ennemi pour l\'affaiblir. Ce Pokémon peut ensuite l\'avaler d\'un coup dans sa gigantesque bouche.',
                types: [ 'Insecte', 'Poison' ],
                PC: 1978,
                health: 187
            },
            {
                index: 4,
                name: 'Salamèche',
                description: 'La flamme qui brûle au bout de sa queue indique l\'humeur de ce Pokémon. Elle vacille lorsque Salamèche est content. En revanche, lorsque qu\'il s\'énerve, la flamme prend de l\'importance et brûle plus ardemment.',
                types: [ 'Feu' ],
                PC: 980,
                health: 103
            },
            {
                index: 15,
                name: 'Dardargnan',
                description: 'Dardargnan est extrêmement possessif. Il vaut mieux ne pas toucher à son nid si on veut éviter d\'avoir des ennuis. Lorsqu\'ils sont en colère, ces Pokémon attaquent en masse.',
                types: [ 'Insecte', 'Poison' ],
                PC: 1846,
                health: 139
            },
            {
                index: 27,
                name: 'Sabelette',
                description: 'Le corps de Sabelette lui permet d\'économiser l\'eau qu\'il absorbe, afin de survivre longtemps dans le désert. Ce Pokémon s\'enroule sur lui-même pour se protéger de ses ennemis.',
                types: [ 'Sol' ],
                PC: 1261,
                health: 118
            },
            {
                index: 232,
                name: 'Donphan',
                description: 'L\'attaque favorite de Donphan consiste à se mettre en boule et à charger son ennemi en roulant à pleine vitesse. Une fois qu\'il commence à rouler, ce Pokémon a du mal à s\'arrêter.',
                types: [ 'Sol' ],
                PC: 3013,
                health: 173
            },
            {
                index: 221,
                name: 'Cochignon',
                description: 'Cochignon est recouvert de longs poils épais qui lui permettent de supporter le froid hivernal. Ce Pokémon utilise ses défenses pour déterrer la nourriture cachée sous la glace.',
                types: [ 'Glace', 'Sol' ],
                PC: 2345,
                health: 187
            },
            {
                index: 195,
                name: 'Maraiste',
                description: 'Maraiste trouve sa nourriture en laissant sa bouche ouverte dans l\'eau, il attend que ses proies y pénètrent sans faire attention. Comme ce Pokémon est inactif, il n\'a jamais très faim.',
                types: [ 'Eau', 'Sol' ],
                PC: 1992,
                health: 180
            },
            {
                index: 170,
                name: 'Loupio',
                description: 'Loupio relâche des charges électriques positives et négatives de ses deux antennes pour mettre sa proie K.O. Ce Pokémon illumine ses ampoules électriques pour communiquer avec ses congénères.',
                types: [ 'Eau', 'Électrik' ],
                PC: 1119,
                health: 153
            },
            {
                index: 171,
                name: 'Lanturn',
                description: 'On surnomme Lanturn « l\'étoile des profondeurs » à cause de son antenne lumineuse. Ce Pokémon produit de la lumière en provoquant une réaction chimique entre des bactéries et les fluides corporels de son antenne.',
                types: [ 'Eau', 'Électrik' ],
                PC: 2085,
                health: 221
            },
            {
                index: 168,
                name: 'Migalos',
                description: 'À l\'extrémité des pattes de Migalos, on trouve de petits crochets qui lui permettent de cavaler sur les surfaces verticales et au plafond. Ce Pokémon peut piéger ses ennemis dans sa solide toile de soie.',
                types: [ 'Insecte', 'Poison' ],
                PC: 1772,
                health: 146
            },
            {
                index: 169,
                name: 'Lanturn',
                description: 'On sait que Lanturn émet de la lumière. Si on regarde attentivement la mer de nuit à partir d\'un bateau, on peut voir la lumière générée par ce Pokémon éclairer les profondeurs. Cela donne à la mer l\'apparence d\'un ciel étoilé.',
                types: [ 'Poison', 'Vol' ],
                PC: 2085,
                health: 221
            },
            {
                index: 166,
                name: 'Coxyclaque',
                description: 'On dit que dans les pays où le ciel est dégagé et où les étoiles illuminent les cieux, vivent d\'innombrables quantités de Coxyclaque. La raison en est simple : l\'énergie de ce Pokémon provient de la lumière des étoiles.',
                types: [ 'Insecte', 'Vol' ],
                PC: 1346,
                health: 125
            },
            {
                index: 174,
                name: 'Toudoudou',
                description: 'Les cordes vocales de Toudoudou ne sont pas assez développées. S\'il devait chanter trop longtemps, il se ferait mal à la gorge. Ce Pokémon se gargarise souvent avec de l\'eau fraîche puisée dans un ruisseau clair.',
                types: [ 'Normal' ],
                PC: 535,
                health: 173
            },
            {
                index: 245,
                name: 'Suicune',
                description: 'Suicune incarne la tranquillité d\'une source d\'eau pure. Il parcourt les plaines avec grâce. Ce Pokémon a le pouvoir de purifier l\'eau.',
                types: [ 'Eau' ],
                PC: 2983,
                health: 187
            },
            {
                index: 243,
                name: 'Raikou',
                description: 'Raikou incarne la vitesse de l\'éclair. Les rugissements de ce Pokémon libèrent des ondes de choc provenant du ciel et frappant le sol avec la puissance de la foudre.',
                types: [ 'Électrik' ],
                PC: 3452,
                health: 173
            }
        ]
    }
}