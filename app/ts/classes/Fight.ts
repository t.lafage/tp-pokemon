import { Pokemon } from "./Pokemon";
import { Tools }   from "./Tools";
import { Player }  from "./Player";


export class Fight {

    private fight       : boolean; //Est-ce que le combat est en cours ?
    private action_time : boolean; //Est-ce qu'une action est en cours ?
    public is_capture   : boolean; //Le pokémon a t il été capturé ?

    public DOMfight     : HTMLDivElement;
    public DOMattack    : HTMLDivElement;
    public DOMdefense   : HTMLDivElement;
    public DOMaction    : HTMLDivElement;

    private player          : Player;
    private pokemon_attack  : Pokemon; //pokémon qui attaque ( lancé par le joueur )
    private pokemon_defense : Pokemon; //pokémon qui défend ( le pokémon que le joueur veut récupérer )

    private health_attack   : number;
    private health_defense  : number;

    private health_attack_decalX : number;
    private health_attack_decalY : number;


    constructor(
        player          : Player,
        pokemon_attack  : Pokemon,
        pokemon_defense : Pokemon
    ){

        this.fight       = true;
        this.action_time = false;
        this.is_capture  = false;

        this.DOMfight    = <HTMLDivElement>document.getElementById('fight-container');
        this.DOMattack   = <HTMLDivElement>document.getElementById('pokemon-attack-image');
        this.DOMdefense  = <HTMLDivElement>document.getElementById('pokemon-defense-image');
        this.DOMaction   = <HTMLDivElement>document.getElementById('pokemon-fight-action');

        this.player          = player;
        this.pokemon_attack  = pokemon_attack;
        this.pokemon_defense = pokemon_defense;

        this.health_attack   = pokemon_attack.health;
        this.health_defense  = pokemon_defense.health;

        this.health_attack_decalX   = Tools.getBackDecalX( pokemon_attack.key );;
        this.health_attack_decalY   = Tools.getBackDecalY( pokemon_attack.key );;

    }
    
    display(): void {

        this.DOMfight.style.display = 'block';

        console.log(this.DOMattack );
        console.log( this.pokemon_attack.decalX /64 );
        console.log( this.pokemon_attack.decalY /64);

        this.DOMattack.style.backgroundPositionX  =  `-${this.health_attack_decalX}px`;
        this.DOMattack.style.backgroundPositionY  =  `-${this.health_attack_decalY}px`;

        this.DOMdefense.style.backgroundPositionX  =  `-${this.pokemon_defense.decalX}px`;
        this.DOMdefense.style.backgroundPositionY  =  `-${this.pokemon_defense.decalY}px`;
    }

    begin(): void {

        this.display();

        console.log( 'Attague : ' + this.pokemon_attack.name + ' ; ' + this.health_attack );
        console.log( 'Défense : ' + this.pokemon_defense.name + ' ; ' + this.health_defense );

        this.DOMaction.addEventListener( 'click', () => {
            if( this.fight && !this.action_time ) {

                this.action_time = true;

                this.health_defense = this.health_defense - this.hit( 10 );

                console.log('vie de la défense : ' + this.health_defense );

                if( this.health_defense <= 0 ) {

                    this.is_capture = true;
                    this.DOMfight.style.display = 'none';
                    this.fight = null;
                    
                    console.log( 'Réussite de la capture');
                    this.player.pokedex.addPokemon( this.pokemon_defense );

                }

                if( this.fight) {
                    setTimeout( () => {

                        this.health_attack = this.health_attack - this.hit( 5 );

                        console.log('vie de l\'attaque : ' + this.health_attack );

                        if( this.health_attack <= 0 ) {

                            this.is_capture = false;
                            this.DOMfight.style.display = 'none';
                            this.fight = null;

                            console.log( 'échec de la capture.');

                        }

                        this.action_time = false;

                    }, 1500);
                }
            }
        });
    }

    hit( resistance: number ): number {

        let dice = Math.floor( Tools.getRandomInt( 5, 6 ) * resistance );
        let dodge = Tools.getRandomIntFloor( 1, 6 ) == 6;

        if( !dodge ) return dice;
        else return 0;

        //TODO opérateur ternaire

    }
}