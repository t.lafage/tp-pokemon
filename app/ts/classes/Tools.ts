import { Data } from "./Data";

/**
 * Cette classe est une collection d'outils personnalisé pour faire des calculs, des opérations...
*/

export class Tools {

    static getRandomInt( min: number, max: number ): number {

        return Math.random() * ( max - min ) + min;

    }

    static getRandomIntFloor( min: number, max: number ): number {

        return Math.floor(Math.random() * ( max - min ) + min);

    }

    static getDecalX( key ): number {

        return ( ( Data.getData()[key].index - 1 ) % 25) * 64;

    }

    static getBackDecalX( key ): number {

        return ( ( Data.getData()[key].index - 1 ) % 31) * 64 + 20;

    }

    static getDecalY( key ): number {

        return Math.floor( (Data.getData()[key].index - 1) / 25) * 64;

    }

    static getBackDecalY( key ): number {

        return Math.floor( (Data.getData()[key].index - 1) / 31) * 64 + 20;

    }

    static degreesToRadians( degrees ): number {

        return degrees * Math.PI / 180;

    }
      
    static distanceInKmBetweenEarthCoordinates( lat1: number, lon1: number, lat2: number, lon2: number ): number {

        var earthRadiusKm: number = 6371;
      
        var dLat = Tools.degreesToRadians( lat2 - lat1 );
        var dLon = Tools.degreesToRadians( lon2 - lon1 );
      
        lat1 = Tools.degreesToRadians( lat1 );
        lat2 = Tools.degreesToRadians( lat2 );
      
        var a = Math.sin( dLat / 2 ) * Math.sin( dLat /2  ) +
                Math.sin( dLon / 2 ) * Math.sin( dLon / 2 ) *
                Math.cos( lat1 ) * Math.cos( lat2 ); 

        var c = 2 * Math.atan2( Math.sqrt( a ), Math.sqrt( 1 - a ) ); 
        
        return earthRadiusKm * c;

    }

    // static getGeolocation() {

    //     navigator.geolocation.getCurrentPosition( ( geopos ) => {
    //             return geopos.coords.latitude;
    //     });

    // }

}