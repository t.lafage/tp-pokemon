import { Pokemon }    from "./Pokemon";
import menu           from "./Menu";
import { Event_list } from "./Event_list";
import { Event }      from "./Event";
import { Player }     from "./Player";

class Game {

    public events             : Array<Event>;
    public players            : Array<Player>;

    public DOMform_new_player : HTMLFormElement;
    public DOMinp_name        : HTMLInputElement;

    public DOMhello           : HTMLDivElement;
    public DOMplayer          : HTMLDivElement;

    public DOMmenu            : HTMLDivElement;
    public DOMmenu_list       : HTMLDivElement;

    public DOMdiv_event       : HTMLDivElement;
    public DOMevent           : HTMLDivElement;
    public DOMlist            : HTMLDivElement;

    public isDisplay          : boolean;

    constructor() {

        this.events             = [];
        this.players            = [];

        this.DOMform_new_player = <HTMLFormElement>document.getElementById('form-new-player');
        this.DOMinp_name        = <HTMLInputElement>document.getElementById('name');

        this.DOMhello           = <HTMLDivElement>document.getElementById('hello');
        this.DOMplayer          = null;

        this.DOMmenu            = <HTMLDivElement>document.getElementById('menu');
        this.DOMmenu_list       = <HTMLDivElement>document.getElementById('menu-list');

        this.DOMevent           = <HTMLDivElement>document.getElementById('event');
        this.DOMdiv_event       = <HTMLDivElement>document.getElementById('event-info');
        this.DOMlist            = <HTMLDivElement>document.getElementById('event-info-list');

        this.isDisplay          = false;

        this.addListeners(); // A l'initialisation du jeu, ajouter les écouteurs sur les éléments visible qui ne sont pas propre au joueur.

    }

    initGame(): void {

        const json_players: Array<Player> = JSON.parse( localStorage.getItem( 'players' ) );
        const json_events: Array<any> = JSON.parse( localStorage.getItem( 'events' ) );

        
        if( json_players ) for ( let str_player of json_players ) this.newPlayer( str_player.name );

        if( json_events ) for( let event of json_events ) {

            var new_date_start: Date = new Date( event.date_start );
            var new_date_end: Date = new Date( event.date_end );

            let new_event: Event = new Event(
                new_date_start,
                new_date_end,
                event.description,
                event.type
            );

            this.addEvent( new_event );

        }

        console.log( 'Evénements dans l\'application' );
        console.log( this.events );

    }

    newPlayer( str_player_name: string ): void {

        let new_player: Player = new Player( str_player_name );

        this.addPlayer( new_player )
        
        let DOMplayer = document.createElement( 'div' );
        DOMplayer.className = 'player';
        DOMplayer.innerHTML = new_player.name;

        DOMplayer.addEventListener( 'click', () => {

            this.DOMhello.style.display = 'none';

            // console.log(new_player);
            
            this.initPlayer( new_player );

        })

        this.DOMhello.append( DOMplayer );

    }

    initPlayer( player: Player ): void {

        const json_pokedex: Array<Pokemon> = JSON.parse( localStorage.getItem( player.name ) );

        if( json_pokedex ) for( let pokemon of json_pokedex ) player.pokedex.addPokemon( pokemon );

        console.log( 'Pokémons dans le pokédex de ' + player.name );
        console.log( player.pokedex.pokemons );

        player.startSession();

    }

    addListeners(): void {

        this.DOMform_new_player.addEventListener( 'submit', (e) =>{

            e.preventDefault();
            
            if( this.DOMinp_name.value ) this.newPlayer( this.DOMinp_name.value );

        });

        menu.addListener();

    }

    addEvent( event: Event ): void {

        this.events.push( event );
        this.save();
        
    }

    addPlayer( player: Player ): void {
        
        this.players.push( player );
        this.save();
        
    }

    rmEvent( key: number ): void {
        
        this.events.splice( key, 1 );
        this.save();

    }

    rmPlayer( key: number ): void {
        
        this.players.splice( key, 1 );
        this.save();

    }

    save(): void {
        
        localStorage.setItem( 'events', JSON.stringify( this.events ) );
        localStorage.setItem( 'players', JSON.stringify( this.players ) );
        
    }
}

export default new Game;