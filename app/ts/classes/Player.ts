import { Position } from "./Position";
import { Spawn } from "./Spawn";
import { Pokemon } from "./Pokemon";
import map from '../classes/Map';
import { Const } from './Const';
import { Pokedex } from "./Pokedex";
import * as mapboxgl from 'mapbox-gl';
import { Tools } from "./Tools";
import { Event_list } from "./Event_list";

/**
 * Cette classe permet d'enregistrer les informations du joueur que l'on enverra au jeu.
 */

export class Player {

    public coord_player  : Position;
    public name          : string;

    public pokedex       : Pokedex;
    public event_list    : Event_list;

    private DOMmarker    : HTMLElement;

    public spawn_interval: number;

    constructor( name: string ) {

        this.coord_player = null;
        this.name = name;

        this.pokedex = new Pokedex( this );
        this.event_list = null;

        this.DOMmarker = null;

        this.spawn_interval = null;
        
    }

    toJSON() {
        return {
            'name': this.name,
            'coord_player': this.coord_player,
            'pokedex': this.pokedex
        };
    }

    startSession(): void {

        setTimeout( () => {

            console.log( 'Lancement du jeu.' );
            
            navigator
                .geolocation
                .getCurrentPosition( ( geopos ) => {

                    // je récupère la géolocalisation du joueur
                    const position = new Position(
                        geopos.coords.latitude,
                        geopos.coords.longitude
                    );
            
                    // j'initialise la carte selon la géolocalisation du joueur
                    map.map
                        .setZoom(19)
                        .setPitch(60)
                        .panTo( position );

                    this.coord_player = position;

                    this.DOMmarker = document.createElement('div');
                    this.DOMmarker.className = `player-marker`;

                    // je place le marqueur du joueur selon sa géolocalisation
                    new mapboxgl.Marker( this.DOMmarker )
                        .setLngLat( this.coord_player )
                        .addTo( map.map );

                    this.event_list = new Event_list( this );

                    //Je lance l'aparition des pokémons
                    new Spawn( this, new Pokemon( 'all', this.coord_player ) );

                    this.spawn_interval = setInterval( () => {
                        
                        new Spawn( this, new Pokemon( 'all', this.coord_player ) );

                    }, Const.SPAWN_RATE );
                });

        }, 5000);

    }
}