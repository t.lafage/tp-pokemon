import { Position } from './Position';
import { Tools } from './Tools';
import { Data } from './Data';

export class Pokemon {

    public type_filtre  : string;
    public position     : Position;
    public key          : number;

    public index        : number;
    public name         : string;
    public description  : string;
    public types        : Array<string>;
    public PC           : number;
    public health       : number;

    public decalX       : number;
    public decalY       : number;
  
    constructor(
        type_filtre : string   = 'all',
        coord_player: Position = new Position( 1, 1 )
    ){

        this.type_filtre = type_filtre;
        this.position = new Position(
            Tools.getRandomInt(
                coord_player.lat - 0.01,
                coord_player.lat + 0.01
            ),
            Tools.getRandomInt(
                coord_player.lng - 0.01,
                coord_player.lng + 0.01
            ),
        )

        if( this.type_filtre == 'all' ) {

            this.key = Tools.getRandomIntFloor( 0, Data.getData().length - 1 ); // Je choisi un pokémon au hasard

            // je récupère les donnée du pokémon instancié.
            this.name        = Data.getData()[this.key].name;
            this.description = Data.getData()[this.key].description;
            this.types       = Data.getData()[this.key].types;
            this.index       = Data.getData()[this.key].index;
            this.PC          = Data.getData()[this.key].PC;
            this.health      = Data.getData()[this.key].health;

            this.decalX = Tools.getDecalX( this.key ); // Je prend son decalage horizontal
            this.decalY = Tools.getDecalY( this.key ); // Je prend son decalage vertical

        }

        else {

            var array: Array<Pokemon> = [];

            for( let pokemon of Data.getData() )
                if( pokemon.types.indexOf(this.type_filtre) >= 0 ) array.push( pokemon );

            this.key = Tools.getRandomIntFloor( 0, array.length - 1 );

            this.name        = array[this.key].name;
            this.description = array[this.key].description;
            this.types       = array[this.key].types;
            this.index       = array[this.key].index;

            this.decalX = ( ( array[this.key].index - 1 ) % 25) * 64;
            this.decalY = Math.floor( ( array[this.key].index - 1) / 25) * 64;

        }
    }
}