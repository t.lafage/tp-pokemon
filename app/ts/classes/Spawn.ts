import * as mapboxgl    from 'mapbox-gl';
import { Tools }        from "./Tools";
import { Pokemon }      from './Pokemon';
import { Player }       from './Player';
import map              from './Map';
import { Const }        from './Const';
import { Pokemon_info } from './Pokemon_info';
import { Fight }        from './Fight';

/**
 * Cette classe renvoi à l'affichage des pokémons sur la carte ( marqueurs personnalisés )
 */

export class Spawn {

    protected popup      : mapboxgl.Popup;

    protected player     : Player;
    protected pokemon    : Pokemon;

    protected layer      : mapboxgl.Marker;

    public DOMpopup      : HTMLDivElement;
    public DOMbtn_capture: HTMLButtonElement;
    public DOMbtn_details: HTMLButtonElement;
    public DOMmarker     : HTMLDivElement;

    constructor( player: Player, pokemon: Pokemon ) {

        this.popup          = new mapboxgl.Popup();

        this.player         = player;
        this.pokemon        = pokemon;

        this.layer          = null;

        this.DOMpopup       = null;
        this.DOMbtn_capture = null;
        this.DOMbtn_details = null;
        this.DOMmarker      = null;

        this.render();
        
    }

    render(): void  { // Apparition des marqueurs personnalisés
        
        if( this.checkDistance() ) {

            console.log( 'Un spawn a eu lieu !' );

            this.createPopup();
            this.addMarker();

        }
    }

    checkDistance(): boolean {

        console.log(this.player.coord_player.lat);
        console.log(this.pokemon.position.lat);

        // je limite la distance entre les coordonnée du joueur et l'apparition des pokémons
        return Tools.distanceInKmBetweenEarthCoordinates(
            this.player.coord_player.lat,
            this.player.coord_player.lng,
            this.pokemon.position.lat,
            this.pokemon.position.lng
        ) < Const.SPAWN_RADIUS ; //SPAWN_RADIUS est la constante configurée dans la classe Const
        
    }

    createPopup(): void { // Parametrage de la petite fenetre au clic sur le marqueur

        this.DOMpopup = document.createElement('div');
        this.DOMpopup.innerHTML = this.pokemon.name;
        
        this.DOMbtn_capture = document.createElement('button');
        this.DOMbtn_capture.textContent = 'Capturer';

        this.DOMbtn_capture.addEventListener( 'click', () => {
            
            const fight = new Fight(
                this.player,
                this.player.pokedex.pokemons[
                    Tools.getRandomIntFloor( 0, this.player.pokedex.pokemons.length )
                ],
                this.pokemon
            );

            fight.begin();

        });

        this.DOMbtn_details = document.createElement('button');
        this.DOMbtn_details.textContent = 'Détails';
        
        this.DOMbtn_details.addEventListener( 'click', () => {

            const pokemon_info = new Pokemon_info( this.pokemon );
            pokemon_info.dpEventInfo();

        });

        this.DOMpopup.append( this.DOMbtn_capture, this.DOMbtn_details );

        this.popup.setDOMContent( this.DOMpopup );

    }

    addMarker(): void  {

        this.DOMmarker = document.createElement('div');
        this.DOMmarker.style.backgroundPositionX = `-${this.pokemon.decalX}px`;
        this.DOMmarker.style.backgroundPositionY = `-${this.pokemon.decalY}px`;
        this.DOMmarker.className = `marker`;

        this.layer = (new mapboxgl.Marker( this.DOMmarker ))
            .setLngLat( this.pokemon.position )
            .setPopup( this.popup )
            .addTo( map.map );

        setTimeout( () => { console.log('Suppression'); this.layer.remove(); }, Const.SPAWN_TERM );

    }
}