import { Pokemon } from "./Pokemon";
import game from "./Game";
import { Event } from "./Event";
import { Const } from "./Const";
import { Spawn } from "./Spawn";
import { Player } from "./Player";

export class Event_list {

    public DOMevent_list : HTMLDivElement;
    public DOMinfo       : HTMLDivElement;
    public DOMlist       : HTMLDivElement;
    public DOMclose      : HTMLDivElement;

    public DOMnew_event  : HTMLSpanElement;
    public DOMstart_event: HTMLButtonElement;

    private player       : Player;

    private isDisplay    : boolean;
    private current_event: Event;

    constructor( player: Player ) {

        this.DOMevent_list  = <HTMLDivElement>document.getElementById('event');
        this.DOMinfo        = <HTMLDivElement>document.getElementById('event-info');
        this.DOMlist        = <HTMLDivElement>document.getElementById('event-info-list');
        this.DOMclose       = <HTMLDivElement>document.getElementById('event-list-close');

        this.DOMnew_event   = null;
        this.DOMstart_event = null;

        this.player = player;

        this.isDisplay     = false;
        this.current_event = null;

        this.addListeners();

    }

    addListeners(): void  {

        this.DOMevent_list.addEventListener( 'click', () =>
            ( !this.isDisplay ) ? this.display() : this.hide() );
        
        this.DOMinfo.addEventListener( 'click', () =>
            ( !this.isDisplay ) ? this.display() : this.hide() );

    }

    display(): void {

        this.DOMclose.addEventListener( 'click', () => this.DOMinfo.style.display = 'none' );

        this.isDisplay = true
        this.DOMinfo.style.display = 'block';
        this.DOMlist.innerHTML = '';

        // console.log(game.events[0].date_start);

        if( game.events ) { // si il y a des événement en cours
            for ( let event of game.events ) { // pour chaque événement

                var str_day_start  : string = event.date_start.getDate().toString();
                var str_month_start: string = ( event.date_start.getMonth() + 1 ).toString();
                var str_day_end    : string = event.date_end.getDate().toString();
                var str_month_end  : string = ( event.date_end.getMonth() + 1 ).toString();

                this.DOMnew_event = document.createElement('p');
                this.DOMnew_event.innerHTML =
                    `Du ${str_day_start}/${str_month_start}
                    au ${str_day_end}/${str_month_end} :
                    élément de type ${event.type}` ;

                /**
                 * Exemple pour event = ???:
                 * Du ${str_day_start}/${str_month_start}
                 * au ${str_day_end}/${str_month_end} :
                 * élément de type ${event.type}
                 */
                
                this.DOMstart_event = document.createElement('button');
                this.DOMstart_event.value = game.events.indexOf( event ).toString();
                this.DOMstart_event.innerHTML = 'Déclencher cet événement';

                this.DOMstart_event.addEventListener( 'click', (e) => {

                    const DOMtarget = e.target as HTMLButtonElement; // DOM du boutton cliqué
                    
                    clearInterval(this.player.spawn_interval);

                // Lance l'événement dans le cas où aucun événement n'est en cours, sionon le fini
                    if( !this.current_event )
                        this.startEvent( DOMtarget );

                    else if( this.current_event )
                        this.finishEvent( DOMtarget );

                });

                this.DOMlist.append( this.DOMnew_event, this.DOMstart_event );
            } 
        }

        this.DOMnew_event   = null;
        this.DOMstart_event = null;

    }

    hide(): void  {

        this.isDisplay = false;
        this.DOMinfo.style.display = 'none';

    }

    startEvent( DOMtarget ): void  {

        console.log(Const.SPAWN_RATE);
        
        this.current_event = game.events[DOMtarget.value];

        const event_type = game.events[DOMtarget.value].type; // type de l'événement sélectionnné
        console.log( 'Déclanchement de l\'événement : ' + DOMtarget.value + ':' + event_type );

        DOMtarget.innerHTML = 'Mettre fin à cet événement';

        this.player.spawn_interval = setInterval( () => {
            new Spawn( this.player, new Pokemon( event_type, this.player.coord_player ) ) }
        , Const.SPAWN_RATE );

    }

    finishEvent( DOMtarget ): void  {
        
        if( this.current_event == game.events[DOMtarget.value] ) {
            
            console.log( 'Fin de l\'événement en cours' + this.current_event.type );

            game.events.splice( game.events.indexOf(this.current_event), 1); // Je supprime l'événement du jeu

            console.log(game.events);

            this.player.spawn_interval = setInterval( () => {
                new Spawn( this.player, new Pokemon( 'all', this.player.coord_player ) ) }, Const.SPAWN_RATE );

            this.current_event = null;

            this.hide();
            
        }
    }
};