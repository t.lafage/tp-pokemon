export class Event {

    public date_start: Date;
    public date_end: Date;
    public description: string;
    public type: string;

    constructor( date_start: Date, date_end: Date, description: string, type: string ) {

        this.date_start = date_start;
        this.date_end = date_end;
        this.description = description;
        this.type = type;
        
    }
}