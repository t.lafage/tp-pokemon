import { Event } from './Event';
import game from './Game';

class Menu {

    public DOMmenu                : HTMLDivElement;
    public DOMmenu_list           : HTMLDivElement;
    public DOMmenu_event_creator  : HTMLDivElement;
    public DOMevent_creator       : HTMLDivElement;
    public DOMform_event_creator  : HTMLDivElement;
    public DOMclose_menu          : HTMLDivElement;
    public DOMclose_event_creator : HTMLDivElement;

    private DOMinp_date_start     : HTMLInputElement;
    private DOMinp_date_end       : HTMLInputElement;
    private DOMinp_descr          : HTMLInputElement;
    private DOMinp_type           : HTMLInputElement;

    private menuIsDisplay         : boolean;
    private eventCreatorIsDisplay : boolean;

    constructor() {

        this.DOMmenu                =  <HTMLDivElement>document.getElementById('menu');
        this.DOMmenu_list           =  <HTMLDivElement>document.getElementById('menu-list');
        this.DOMmenu_event_creator  =  <HTMLDivElement>document.querySelector('#menu-list > :nth-child(2)');
        this.DOMevent_creator       =  <HTMLDivElement>document.getElementById('event-creator');
        this.DOMform_event_creator  =  <HTMLDivElement>document.getElementById('form-event-creator');
        this.DOMclose_menu          =  <HTMLDivElement>document.getElementById('menu-close');
        this.DOMclose_event_creator =  <HTMLDivElement>document.getElementById('event-creator-close');

        this.DOMinp_date_start =  <HTMLInputElement>document.getElementById('date-start');
        this.DOMinp_date_end   =  <HTMLInputElement>document.getElementById('date-end');
        this.DOMinp_descr      =  <HTMLInputElement>document.getElementById('description');
        this.DOMinp_type       =  <HTMLInputElement>document.getElementById('type');

        this.menuIsDisplay         = false;
        this.eventCreatorIsDisplay = false;

    }

    addListener(): void {

        this.DOMmenu.addEventListener( 'click' , () => {

            if( !this.menuIsDisplay ) { this.displayMenu(); }
            else if( this.menuIsDisplay ) { this.hideMenu() } });

    }

    hideMenu(): void {

        this.menuIsDisplay              = false;
        this.DOMmenu_list.style.display = 'none';

    }
    
    displayMenu(): void {

        this.DOMmenu_event_creator.addEventListener( 'click', () => {

            this.DOMinp_date_start.min = new Date().toISOString().split('T')[0];
            this.DOMinp_date_end.min = new Date().toISOString().split('T')[0];

            if( !this.eventCreatorIsDisplay ) { this.displayEventCreator(); }
            else if( this.eventCreatorIsDisplay ) { this.hideEventCreator()}
            
        });

        this.DOMclose_menu.addEventListener( 'click', () => {
            this.DOMmenu_list.style.display = 'none';
            this.DOMevent_creator.style.display = 'none';
        });

        this.menuIsDisplay              = true;
        this.DOMmenu_list.style.display = 'block';

    }

    displayEventCreator(): void {

        this.DOMform_event_creator.addEventListener( 'submit', (e) => {

            e.preventDefault();

            const date_start  : Date    = new Date( this.DOMinp_date_start.value);
            const date_end    : Date    = new Date( this.DOMinp_date_end.value );
            const description : string  = this.DOMinp_descr.value;
            const type        : string  = this.DOMinp_type.value;

            const event       : Event   = new Event( date_start, date_end, description, type );

            game.addEvent( event );
            
        });

        this.DOMclose_event_creator.addEventListener( 'click',
            () => this.DOMevent_creator.style.display = 'none' );

        this.eventCreatorIsDisplay          = true;
        this.DOMevent_creator.style.display = 'block';

    }

    hideEventCreator(): void {

        this.eventCreatorIsDisplay          = false;
        this.DOMevent_creator.style.display = 'none';

    }

}

export default new Menu;