import { Pokemon } from "./Pokemon";
import { Player }  from "./Player";

export class Pokedex {
    
    public player       : Player;
    public pokemons     : Array<Pokemon>;

    public DOMpokedex   : HTMLDivElement;

    public DOMinfo      : HTMLDivElement;
    public DOMinfo_list : HTMLDivElement;
    public DOMclose     : HTMLDivElement;

    private isDisplay   : boolean;

    constructor( player: Player ) {

        this.player       = player;
        this.pokemons     = [];

        this.DOMpokedex   = <HTMLDivElement>document.getElementById('pokedex');
        this.DOMpokedex.addEventListener( 'click', () =>
            ( !this.isDisplay ) ? this.display() :this.hide() );

        this.DOMinfo      = <HTMLDivElement>document.getElementById('pokedex-info');
        this.DOMinfo_list = <HTMLDivElement>document.getElementById('pokedex-info-list');
        this.DOMclose     = <HTMLDivElement>document.getElementById('pokedex-close');

        this.isDisplay    = false;

    }

    toJSON(): Object {
        return {
            'pokemons': this.pokemons
        };
    }

    addPokemon( pokemon: Pokemon ): void {

        this.pokemons.push( pokemon );
        this.save();

        if( this.isDisplay ) { this.display(); };
        
    }

    display(): void { // j'affiche la fenêtre du pokédex

        this.DOMclose.addEventListener( 'click', () => this.DOMinfo.style.display = 'none' );

        this.isDisplay = true
        this.DOMinfo.style.display = 'block';
        var str_pokemons: Array<string> = [];

        for (let pokemon of this.pokemons)
            str_pokemons.push( pokemon.name + ' : ' + pokemon.PC + ' PC');
        
        // exemple de ligne = 'Migalos : 1772 PC'

        this.DOMinfo_list.innerHTML = str_pokemons.join('<br>');

        // j'affiche les pokémons avec un <br> entre chaque ligne

    }

    hide(): void { // je cache la fenêtre du pokédex

        this.isDisplay = false;
        this.DOMinfo.style.display = 'none';

    }

    save(): void {
        /**
         * Actualisation du Local Storage.
         * 
         * Je crée une nouvelle clé avec le nom du joueur et j'y insère
         * les pokémons présent dans l'application.
         */
        
        localStorage.setItem( this.player.name, JSON.stringify( this.pokemons ) );
        
    }
}