import { Pokemon } from "./Pokemon";

export class Pokemon_info {

    protected pokemon          : Pokemon;

    public DOMinfo             : HTMLDivElement;
    public DOMinfo_image       : HTMLDivElement;
    public DOMinfo_title       : HTMLDivElement;
    public DOMinfo_description : HTMLDivElement;
    public DOMinfo_categories  : HTMLDivElement;
    public DOMclose            : HTMLDivElement;

    constructor( pokemon: Pokemon ) {

        this.pokemon             =  pokemon;

        this.DOMinfo             =  <HTMLDivElement>document.getElementById('pokemon-info');
        this.DOMinfo_image       =  <HTMLDivElement>document.getElementById('pokemon-info-image');
        this.DOMinfo_title       =  <HTMLDivElement>document.getElementById('pokemon-info-title');
        this.DOMinfo_description =  <HTMLDivElement>document.getElementById('pokemon-info-description');
        this.DOMinfo_categories  =  <HTMLDivElement>document.getElementById('pokemon-info-categories');
        this.DOMclose            =  <HTMLDivElement>document.getElementById('pokemon-close');

    }

    dpEventInfo(): void {

        this.DOMclose.addEventListener( 'click', () => this.DOMinfo.style.display = 'none' );

        this.DOMinfo.style.display                    =  'block';

        this.DOMinfo_image.style.backgroundPositionX  =  `-${this.pokemon.decalX}px`;
        this.DOMinfo_image.style.backgroundPositionY  =  `-${this.pokemon.decalY}px`;

        this.DOMinfo_title.innerText                  =  this.pokemon.name;
        this.DOMinfo_description.innerText            =  this.pokemon.description;
        this.DOMinfo_categories.innerText             =  this.pokemon.types.join(', ');

    }

}