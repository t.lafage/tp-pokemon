const path = require('path');

const CONFIG = {

    mode: 'development',

    devtool: 'inline-source-map',

    entry: './app/ts/app.ts',
    output: {
        path: path.resolve( __dirname, 'dist' ),
        filename: 'bundle.js'
    },

    resolve: {
        extensions: ['.js', '.json', '.scss', '.html', '.ts']
    },

    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [
                    'style-loader',
                    {
                        'loader': 'css-loader',
                        options: { sourceMap: true }
                    },
                    {
                        'loader': 'sass-loader',
                        options: { sourceMap: true }
                    }
                ]
            },
            {
                test: /\.html$/,
                use: 'html-loader'
            },
            {
                test: /\.ts$/,
                use: 'ts-loader'
            },
            {
                test: /\.(png|jpg|jpeg|gif)$/,
                use: 'file-loader'
            }
        ]
    },

    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port:9000
    }

};

module.exports = (function(env){

    //Récupère le --env de la command line
    if( env && env.production == 'true' ) {
        CONFIG.mode = 'production';
        CONFIG.devtool = false;
    }

    return CONFIG;

});

//npm start : lancer le serveur de developpement
//npm run prod : générer les fichier minifiés de production
//npm run dev : générer les fichiers non minifiés